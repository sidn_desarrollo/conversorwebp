const gulp  = require('gulp');
const watch = require('gulp-watch');
const webp  = require('gulp-webp');

/**
 * Optimiza las imágenes y crea versiones webp de las mismas.
 */
 let conversorWebp = async () => {
    await gulp.src('./img/**')
        .pipe(webp({quality: 100}))     // Compresión sin pérdidas, si se quiere assets más reducidos se puede jugar con este valor en detrimento de la calidad de la imagen.
        .pipe(gulp.dest('./img/'))
};

// Tarea
gulp.task('webp', conversorWebp );
