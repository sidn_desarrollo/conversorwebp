**INSTRUCCIONES**

1º Es necesario tener instalado node en el entorno donde se ejecute este script (https://nodejs.org/es/)

2º Una vez decargado el repositorio, abrimos el terminal y en la raiz se ejecuta:

`npm install`

3º Una vez instalado todas las dependecias, para ejecutar el optimizador se usa:

`npm run conversor-webp`

De esta forma se convertiran todos los assets que haya en cualquier directorio dentro de .img/ generando el webp homólogo en la misma ruta que el asset original. (en este repo hay algunos ejemplos añadidos para que se pueda hacer pruebas)
